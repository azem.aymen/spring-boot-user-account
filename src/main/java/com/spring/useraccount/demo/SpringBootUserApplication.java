package com.spring.useraccount.demo;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.spring.useraccount.demo.model.User;
import com.spring.useraccount.demo.repository.UserRepository;
import com.spring.useraccount.demo.utils.HelperUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Azem Aymen 
 */ 
@Slf4j
@SpringBootApplication
@EnableJpaRepositories
public class SpringBootUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootUserApplication.class, args);
	}

	private final UserRepository userRepository;

	public SpringBootUserApplication(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Bean
	CommandLineRunner runner() {
		return args -> {
			List<User> users = userRepository.findAll();
			if (users.isEmpty()) {
				log.info("******* Inserting Users to DB *******");
				userRepository.saveAll(HelperUtil.userSupplier.get());
			} else {
				log.info("******* Users stored in DB Size :: {}", users.size());
				log.info("******* Users stored in DB :: {}", users);
			}
		};
	}

}
