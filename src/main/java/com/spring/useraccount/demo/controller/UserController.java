package com.spring.useraccount.demo.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.spring.useraccount.demo.annotation.LogObjectAfter;
import com.spring.useraccount.demo.annotation.LogObjectBefore;
import com.spring.useraccount.demo.dto.UserDTO;
import com.spring.useraccount.demo.service.UserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * Controller for generate management user account APIs .
 * 
 * @author Azem Aymen
 */
@RestController
@RequestMapping("/user")
public class UserController {

	private final UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Return the user account by id.
	 * 
	 * @param id of user
	 */
	@LogObjectAfter
	@GetMapping("/{id}")
	@Operation(summary = "Get user by user id", responses = {
			@ApiResponse(responseCode = "200", description = "The user", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserDTO.class))),
			@ApiResponse(responseCode = "404", description = "User not found") })
	public ResponseEntity<UserDTO> findById(
			@Parameter(example = "1", description = "The id that needs to be fetched. Use 1 for testing. ", required = true) @PathVariable("id") long id) {
		UserDTO user = userService.getUserById(id);
		return ResponseEntity.ok().body(user);
	}

	/**
	 * Save and return the user.
	 * 
	 * @param UserDTO
	 */
	@LogObjectBefore
	@LogObjectAfter
	@PostMapping
	@Operation(summary = "Save user", responses = {
			@ApiResponse(responseCode = "201", description = "The user created", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserDTO.class))),
			@ApiResponse(responseCode = "400", description = "Invalid inputs"),
			@ApiResponse(responseCode = "409", description = "Illegal business logique exception"),
			@ApiResponse(responseCode = "500", description = "Server side exception") })
	public ResponseEntity<UserDTO> save(@Valid @RequestBody UserDTO userDTO) {
		UserDTO user = userService.saveUser(userDTO);
		URI uri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{id}").buildAndExpand(user.getId())
				.toUri();
		return ResponseEntity.created(uri).body(user);
	}

}
