package com.spring.useraccount.demo.aop;

import java.util.Objects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Azem Aymen
 */
@Slf4j
@Aspect
@Component
public class LoggerAspect {

	@Before("@annotation(com.spring.useraccount.demo.annotation.LogObjectBefore)")
	public void logUserBefore(JoinPoint joinPoint) {
		log.info("******* Class :: {}", joinPoint.getSignature().getDeclaringTypeName());
		Object[] args = joinPoint.getArgs();
		for (Object arg : args) {
			log.info("******* Method {} with inputs :: {}", joinPoint.getSignature().getName(), arg);
		}
	}

	@AfterReturning(value = "@annotation(com.spring.useraccount.demo.annotation.LogObjectAfter)", returning = "result")
	public void logUserAfter(JoinPoint joinPoint, Object result) {
		if (Objects.nonNull(result)) {
			if (result instanceof ResponseEntity) {
				ResponseEntity<?> responseEntity = (ResponseEntity<?>) result;
				if (responseEntity.getStatusCode().value() == 200)
					log.info("******* Returning Ok :  outputs :: {}", responseEntity.getBody());
				else if (responseEntity.getStatusCode().value() == 201)
					log.info("******* Returning Created : outputs :: {}", responseEntity.getBody());
				else
					log.error("Something went wrong while logging...!");
			} else {
				Object[] args = joinPoint.getArgs();
				for (Object arg : args) {
					log.info("******* Method {} : outputs :: {}", joinPoint.getSignature().getName(), arg);
				}
			}
		}
	}
}
