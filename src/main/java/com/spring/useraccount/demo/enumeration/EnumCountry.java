package com.spring.useraccount.demo.enumeration;

import java.util.Arrays;
import java.util.Optional;

/**
 * Class for Enumerate the country of residence.
 * 
 * @author Azem Aymen
 */
public enum EnumCountry {
	FRENCH("FRENCH");

	private String name;

	public String getCountry() {
		return name;
	}

	EnumCountry(String country) {
		this.name = country;
	}

	public static Optional<EnumCountry> getCountryFromString(String value) {
		return Arrays.stream(EnumCountry.values()).filter(country -> country.name.equalsIgnoreCase(value)).findFirst();

	}
}
