package com.spring.useraccount.demo.errors;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Azem Aymen
 */
@Getter
@AllArgsConstructor
public class ErrorMessage {

	private int statusCode;
	private LocalDateTime time;
	private String message;

}