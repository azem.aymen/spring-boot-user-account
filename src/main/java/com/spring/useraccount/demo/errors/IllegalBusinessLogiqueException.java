package com.spring.useraccount.demo.errors;

/**
 * Personal Exception to handle Illegal business logique 
 * @author Azem Aymen
 */
public final class IllegalBusinessLogiqueException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public IllegalBusinessLogiqueException(final String message) {
		super(message);
	}

}
