package com.spring.useraccount.demo.errors;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;

/**
 * Class for the ExceptionHandler
 * 
 * @author Azem Aymen
 */
@ControllerAdvice
public class ValidationHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorMessage> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
		String description = "";
		FieldError er = ex.getBindingResult().getFieldError();
		if (null != er && null != er.getDefaultMessage()) {
			description = description.concat(er.getDefaultMessage());
		}
		ErrorMessage message = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), LocalDateTime.now(), description);
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<ErrorMessage> resourceNotFoundException(ResourceNotFoundException ex) {
		ErrorMessage message = new ErrorMessage(HttpStatus.NOT_FOUND.value(), LocalDateTime.now(), ex.getMessage());
		return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorMessage> globalExceptionHandler(Exception ex) {
		ErrorMessage message = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), LocalDateTime.now(),
				"Server side exception");
		return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<ErrorMessage> httpMessageNotReadableException(HttpMessageNotReadableException ex) {
		String msg = ex.getMessage();
		Throwable cause = ex.getCause();
		if (cause instanceof JsonParseException) {
			JsonParseException jpe = (JsonParseException) cause;
			msg = jpe.getOriginalMessage();
		} else if (cause instanceof MismatchedInputException) {
			MismatchedInputException mie = (MismatchedInputException) cause;
			if (mie.getPath() != null && !mie.getPath().isEmpty()) {
				msg = "Invalid request field: " + mie.getPath().get(0).getFieldName();
			}
		} else if (cause instanceof JsonMappingException) {
			JsonMappingException jme = (JsonMappingException) cause;
			msg = jme.getOriginalMessage();
			if (jme.getPath() != null && !jme.getPath().isEmpty()) {
				msg = "Invalid request field: " + jme.getPath().get(0).getFieldName() + ": " + msg;
			}
		}
		ErrorMessage message = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), LocalDateTime.now(), msg);
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IllegalBusinessLogiqueException.class)
	public ResponseEntity<ErrorMessage> businessExceptionHandler(IllegalBusinessLogiqueException ex) {
		ErrorMessage message = new ErrorMessage(HttpStatus.CONFLICT.value(), LocalDateTime.now(), ex.getMessage());
		return new ResponseEntity<>(message, HttpStatus.CONFLICT);
	}

}