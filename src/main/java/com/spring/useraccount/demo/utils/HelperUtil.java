package com.spring.useraccount.demo.utils;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import com.spring.useraccount.demo.enumeration.EnumCountry;
import com.spring.useraccount.demo.enumeration.EnumGender;
import com.spring.useraccount.demo.model.User;

/**
 * @author Azem Aymen
 */
public class HelperUtil {

	private HelperUtil() {
	}

	public static final Supplier<List<User>> userSupplier = () -> Arrays.asList(
			User.builder().name("Aymen").gender(EnumGender.M).birthDate(LocalDate.of(1990, 12, 10))
					.phoneNumber("0618303989").countryResidence(EnumCountry.FRENCH).build(),
			User.builder().name("Philippe").gender(EnumGender.M).birthDate(LocalDate.of(1990, 12, 10))
					.phoneNumber("0707078099").countryResidence(EnumCountry.FRENCH).build()

	);

}
