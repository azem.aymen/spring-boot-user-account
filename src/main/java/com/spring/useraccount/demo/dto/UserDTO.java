package com.spring.useraccount.demo.dto;

import java.time.LocalDate;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.spring.useraccount.demo.enumeration.EnumCountry;
import com.spring.useraccount.demo.enumeration.EnumGender;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Azem Aymen
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDTO {
	@Schema(hidden = true)
	private Long id;

	@NotNull(message = "user name is required")
	@Size(min = 2, message = "user name should have at least 2 characters")
	@Schema(type = "string", example = "Philippe")
	private String name;

	@NotNull(message = "user birth date is required")
	@Schema(type = "localdate", example = "2001-01-01")
	private LocalDate birthDate;

	@NotNull(message = "user country is required")
	@Enumerated(EnumType.STRING)
	private EnumCountry countryResidence;

	@Schema(type = "String", example = "0701010209")
	private String phoneNumber;

	@Enumerated(EnumType.STRING)
	private EnumGender gender;

}
