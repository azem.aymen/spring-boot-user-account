package com.spring.useraccount.demo.service;

import com.spring.useraccount.demo.dto.UserDTO;

/**
 * interface to define the user service methods.
 * 
 * @author Azem Aymen
 */
public interface UserService {

	UserDTO getUserById(Long id);

	UserDTO saveUser(UserDTO dto);

}
