package com.spring.useraccount.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.useraccount.demo.model.User;

/**
 * @author Azem Aymen
 */
public interface UserRepository extends JpaRepository<User, Long> {

}
