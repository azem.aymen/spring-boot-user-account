package com.spring.useraccount.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.spring.useraccount.demo.dto.UserDTO;
import com.spring.useraccount.demo.enumeration.EnumCountry;

/**
 * @author Azem Aymen
 */
@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
class UserControllerTest {

	@LocalServerPort
	private int port;

	private String baseUrl = "http://localhost";

	private static RestTemplate restTemplate;

	private UserDTO dto = UserDTO.builder().name("Aymen").birthDate(LocalDate.of(1990, 12, 29))
			.countryResidence(EnumCountry.FRENCH).build();

	@BeforeAll
	public static void init() {
		restTemplate = new RestTemplate();
	}

	@BeforeEach
	public void setUp() {
		baseUrl = baseUrl.concat(":").concat(String.valueOf(port)).concat("/user");
	}

	@Test
	@Order(1)
	@DisplayName("get user ok")
	void testGetUserOk() {
		ResponseEntity<UserDTO> response = restTemplate.getForEntity(baseUrl + "/{id}", UserDTO.class, 1L);
		assertEquals(200, response.getStatusCodeValue());
		assertThat(response.getBody());
		assertEquals(EnumCountry.FRENCH, response.getBody().getCountryResidence());
	}

	@Test
	@Order(2)
	@DisplayName("save new user ok")
	void testSaveUserOk() {
		ResponseEntity<UserDTO> response = restTemplate.postForEntity(baseUrl, dto, UserDTO.class);
		assertEquals(201, response.getStatusCodeValue());
		assertThat(response.getBody());
		assertEquals(EnumCountry.FRENCH, response.getBody().getCountryResidence());
	}

	@Test
	@Order(3)
	@DisplayName("get user throw NotFound")
	void testGetUserKo_NotFound() {
		assertThrows(HttpClientErrorException.NotFound.class,
				() -> restTemplate.getForEntity(baseUrl + "/{id}", UserDTO.class, 10L));
	}

	@Test
	@Order(4)
	@DisplayName("save new user throw Conflict")
	void testSaveUserKO_Conflict() throws Exception {
		UserDTO mockuserDTO = UserDTO.builder().name("Aymen").birthDate(LocalDate.of(2020, 12, 29))
				.countryResidence(EnumCountry.FRENCH).build();
		assertThrows(HttpClientErrorException.Conflict.class,
				() -> restTemplate.postForEntity(baseUrl, mockuserDTO, UserDTO.class));
	}

	@Test
	@Order(5)
	@DisplayName("save new user throw BadRequest")
	void testSaveUserKO_BadRequest() throws Exception {
		UserDTO mockuserDTO = UserDTO.builder().name("Aymen").countryResidence(EnumCountry.FRENCH).build();
		assertThrows(HttpClientErrorException.BadRequest.class,
				() -> restTemplate.postForEntity(baseUrl, mockuserDTO, UserDTO.class));
	}

}